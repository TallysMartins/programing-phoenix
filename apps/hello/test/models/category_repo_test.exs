defmodule Hello.CategoryRepoTest do
  use Hello.ModelCase

  alias Hello.Category

  test "alphabetical/1 orders by name" do
    Repo.insert!(%Category{name: "c"})
    Repo.insert!(%Category{name: "a"})
    Repo.insert!(%Category{name: "b"})

    query = Category |> Category.alphabetical()
    query = from c in query, select: c.name
    assert Repo.all(query) == ~w(a b c)
  end

  test "names_and_ids/1 return name and id" do
    category = Repo.insert!(%Category{name: "c"})
  
    query = Category |> Category.names_and_ids()
    assert {"c", category.id} in Repo.all(query)
  end
end
