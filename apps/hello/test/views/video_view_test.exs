defmodule Hello.VideoViewTest do
  use Hello.ConnCase, async: true
  import Phoenix.View

  test "renders index.html", %{conn: conn} do
    videos = [%Hello.Video{id: "1", title: "dogs"},
              %Hello.Video{id: "2", title: "cats"}]
    content = render_to_string(Hello.VideoView, "index.html",
      conn: conn, videos: videos)

    assert String.contains?(content, "Listing videos")
    for video <- videos do
      assert String.contains?(content, video.title)
    end
  end

  test "renders new.html", %{conn: conn} do
    changeset = Hello.User.changeset(%Hello.User{}) 
    categories = [{"cats", 123}]
    content = render_to_string(Hello.VideoView, "new.html",
      conn: conn, changeset: changeset, categories: categories)
  end
end
