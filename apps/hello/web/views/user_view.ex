defmodule Hello.UserView do
  use Hello.Web, :view
  alias Hello.User

  def first_name(%User{name: name}) do
    name
    |> String.split(" ")
    |> Enum.at(0)
  end

  def first_name(_) do
    "Anonynmous"
  end

  def render("user.json", %{user: user}) do
    %{id: user.id, username: user.username}
  end
end
