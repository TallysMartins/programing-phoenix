defmodule Hello.AnnotationView do
  use Hello.Web, :view

  def render("annotation.json", %{annotation: ann}) do
    %{
      id: ann.id,
      body: ann.body,
      at: ann.at,
      user: render_one( ann.user, Hello.UserView, "user.json")
    }
  end
end
