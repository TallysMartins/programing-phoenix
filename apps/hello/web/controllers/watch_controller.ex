defmodule Hello.WatchController do
  use Hello.Web, :controller
  alias Hello.Video

  def index(conn, _params) do

  end

  def show(conn, %{"id" => id}) do
    video = Repo.get(Video, id)
    render conn, "show.html", video: video
  end
end
