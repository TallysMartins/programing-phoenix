defmodule InfoSys.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      worker(InfoSys, [], restart: :temporary),
    ]

    opts = [strategy: :simple_one_for_one, name: InfoSys.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
